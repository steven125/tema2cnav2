﻿﻿using Generated;
using Grpc.Core;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Server
{
    internal class ZodiacService : Generated.ZodiacService.ZodiacServiceBase
    {
        public override Task<ZodiacResponse> GetSign(ZodiacRequest request, ServerCallContext context)
        {
            Console.WriteLine(request.Date);
            var result = new ZodiacResponse();
            result.Sign = AnalyzeDate(request.Date);
            return Task.FromResult(result);
        }

        private List<Interval> LoadFromFile()
        {
            string[] lines = System.IO.File.ReadAllLines("..//Zodiacs.txt");
            List<Interval> list = new List<Interval>();
            foreach(string str in lines)
            {
                var split = Regex.Split(str, " ");
                list.Add(new Interval { Sign = split[0], StartingDate = split[1], Ending = split[2]});
            }
            return list;
        }

        private Tuple<int, int> GetDateFromString(string input)
        {
            var split = Regex.Split(input, @"/");
            var tuple = new Tuple<int, int>(int.Parse(split[0]), int.Parse(split[1]));
            return tuple;
        }

        private string AnalyzeDate(string date)
        {
            List<Interval> list = LoadFromFile();
            for (int index = 0; index < list.Count; ++index)
            {
                var starting = GetDateFromString(list[index].StartingDate);
                var ending = GetDateFromString(list[index].Ending);
                var current = GetDateFromString(date);
                if (IsInInterval(starting, ending, current))
                {
                    return list[index].Sign;
                }
            }
            return "Unknown sign";
        }

       private Boolean IsInInterval(Tuple<int, int> starting, Tuple<int, int> ending, Tuple<int, int> current)
        {

            if(starting.Item1 == current.Item1)
            {
                if(starting.Item2 <= current.Item2 && current.Item2 <= 31)
                {
                    return true;
                }
            }
            else if(ending.Item1 == current.Item1)
            {
                if(current.Item2 >= 01 && current.Item2 <= ending.Item2)
                {
                    return true;
                }
            }
            return false;
        }
    }

}
