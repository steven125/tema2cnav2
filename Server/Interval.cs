﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Interval
    {
		private string sign;

		public string Sign
		{
			get { return  sign; }
			set {  sign = value; }
		}

		private string startingDate;

		public string StartingDate
		{
			get { return startingDate; }
			set { startingDate = value; }
		}

		private string ending;

		public string Ending
		{
			get { return ending; }
			set { ending = value; }
		}
	}
}

	