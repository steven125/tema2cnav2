﻿using Grpc.Core;
using System;
using System.Globalization;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 16842;

            var channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);

            Console.WriteLine("Enter your date: ");
            string date = Console.ReadLine();
            try
            {
                DateTime.ParseExact(date, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                var client = new Generated.ZodiacService.ZodiacServiceClient(channel);
                var response = client.GetSign(new Generated.ZodiacRequest() { Date = date });
                Console.WriteLine(response);
            }
            catch(Exception e)
            {
                Console.WriteLine("Invalid date");
            }

            // Shutdown
            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
